import logo from './logo.svg';
import './App.css';
import PhotoApp from "./components/PhotoApp";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Photo from "./components/Photo";

function App() {
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path="/">
                        <PhotoApp/>
                    </Route>
                    <Route path="/photo/:code">
                        <Photo ></Photo>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;