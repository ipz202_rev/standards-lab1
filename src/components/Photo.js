import React, {useState, useEffect} from "react";
import {Image} from "react-bootstrap";
import {useParams} from "react-router-dom";

function Photo() {
    const {code} = useParams();
    let url = "https://via.placeholder.com/600/" + code;

    return (
        <div>
            <Image src={url}  />
        </div>
    );


}

export default Photo;