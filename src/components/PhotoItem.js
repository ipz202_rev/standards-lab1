import React, {useState, useEffect} from "react";
import {Image} from "react-bootstrap";
import {Link} from "react-router-dom";

function PhotoItem(props) {
    let code =  (props.photo.url).split('/');
    code = code[code.length - 1];

    return (
        <div className="list-group-item d-flex align-items-center p-2">
            <div className="mx-2">
                <Link to={`/photo/${code}`}>
                    <Image roundedCircle={true} src={props.photo.thumbnailUrl} style={{height: props.height}}/>
                </Link>
            </div>
            <div className="mx-2">
                {props.photo.title}
            </div>
        </div>
    );


}

export default PhotoItem;