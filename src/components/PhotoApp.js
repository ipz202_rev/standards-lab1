import React, {useState, useEffect} from "react";
import PhotoItem from "./PhotoItem";
import Container from "react-bootstrap/container";
import {AutoSizer, List} from "react-virtualized";
import {Col, Form, Row} from "react-bootstrap";

function PhotoApp() {
    const [photos, setPhotos] = useState(null);
    const [list, setList] = useState(null);
    const [loading, setLoading] = useState(true);
    const [listHeight, setListHeight] = useState(700);
    const [rowHeight, setRowHeight] = useState(50);
    const [rowCount, setRowCount] = useState(1000);
    const [pageRowCount, setPageRowCount] = useState(10);
    const [prev, setPrev] = useState(true);
    const [next, setNext] = useState(false);
    const [page, setPage] = useState(0);
    const [nameSort, setNameSort] = useState('asc');
    const [albumSort, setAlbumSort] = useState('none');
    const [findName, setFindName] = useState('');
    const [findAlbum, setFindAlbum] = useState('');

    useEffect(() => {
        //fetchPhotos(rowCount).then(() => {setLoading(false)});
        find();
    }, [findName, findAlbum]);

    useEffect(() => {
        fetchPhotos().then(() => {setLoading(false)});
    }, []);

    useEffect(() => {
        setPage(0);
    }, [rowCount]);

    const fetchPhotos = async () => {
        let response = await fetch("https://jsonplaceholder.typicode.com/photos?_limit=" + rowCount);
        let photos = await response.json();
        setPhotos(photos);
        setList(photos);
    }

    const prevPage = () => {
        const temp = page - 1;
        let max = Math.ceil(rowCount/pageRowCount);
        if (temp <= 0) {
            setPage(0);
            setPrev(true);
        }
        else {
            setPage(temp);
            setPrev(false)
        }
        if (temp < max - 1) {
            setNext(false);
        }
        else {
            setNext(true);
        }
    }

    const nextPage = () => {
        const temp = page + 1;
        let max = Math.ceil(rowCount/pageRowCount);
        console.log(max, rowCount, pageRowCount)
        if (temp < max - 1) {
            setPage(temp);
            setNext(false);
        }
        else {
            setPage(temp);
            setNext(true);
        }
        if (temp === 0) {
            setPrev(true);
        }
        else {
            setPrev(false)
        }
    }

    function sortByName() {
        switch (nameSort) {
            case 'asc':
                setNameSort('desc');
                setList(list.sort((a, b) => a.title.split("/")[0] < b.title.split("/")[0] ? 1 : -1));
                break;
            case 'desc':
                setNameSort('asc');
                setList(list.sort((a, b) => a.title.split("/")[0] > b.title.split("/")[0] ? 1 : -1));
                break;
            default:
                break;
        }
    }

    function sortByAlbum() {
        switch (albumSort) {
            case 'none':
                setAlbumSort('asc');
                setList(list.sort((a, b) => parseInt(a.albumId) < parseInt(b.albumId) ? 1 : -1));
                break;
            case 'asc':
                setAlbumSort('desc');
                setList(list.sort((a, b) => parseInt(a.albumId) > parseInt(b.albumId) ? 1 : -1));
                break;
            case 'desc':
                setAlbumSort('asc');
                setList(list.sort((a, b) => parseInt(a.albumId) < parseInt(b.albumId) ? 1 : -1));
                break;
            default:
                break;
        }
    }

    function find() {
        if (photos !== null) {
            setPage(0);

            let temp;
            if (findName !== '' && findAlbum !== '') {
                temp = photos.filter(item => item.title.includes(findName) && item.albumId == findAlbum);
            }
            else if (findName !== '' && findAlbum === '') {
                temp = photos.filter(item => item.title.includes(findName));
            }
            else if (findName === '' && findAlbum !== '') {
                temp = photos.filter(item => item.albumId == findAlbum);
            }
            else {
                temp = photos;
            }

            setList(temp);
            setRowCount(temp.length ? temp.length : 0);

            console.log(findName, rowCount, pageRowCount, temp.length)
        }
    }

    return (
        <Container>
            {
                loading ? <div>Loading</div> :
                    <div style={{height: '80vh'}}>
                        <Form>
                            <Row className="mb-3">
                                <Form.Group as={Col}>
                                    <Form.Label>List Height</Form.Label>
                                    <Form.Control placeholder="listHeight" onChange={
                                        e => {setListHeight(!isNaN(parseInt(e.target.value)) ? parseInt(e.target.value) : 700)}
                                    } ></Form.Control>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Row Height</Form.Label>
                                    <Form.Control placeholder="rowHeight" onChange={
                                        e => {setRowHeight(!isNaN(parseInt(e.target.value)) ? parseInt(e.target.value) : 50)}
                                    }></Form.Control>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Row Count</Form.Label>
                                    <Form.Control placeholder="rowCount" onChange={
                                        e => {setRowCount(!isNaN(parseInt(e.target.value)) ? parseInt(e.target.value) : 0)}
                                    }></Form.Control>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Row Count on Page</Form.Label>
                                    <Form.Control placeholder="pageRowCount" onChange={
                                        e => {setPageRowCount(!isNaN(parseInt(e.target.value)) ? parseInt(e.target.value) : 0)}
                                    }></Form.Control>
                                </Form.Group>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col}>
                                    <Form.Label>Find by Name</Form.Label>
                                    <Form.Control placeholder="findName" value={findName} onChange={
                                        e => {setFindName(e.target.value)}
                                    }></Form.Control>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Find by Album</Form.Label>
                                    <Form.Control placeholder="findAlbum" value={findAlbum} onChange={
                                        e => {setFindAlbum(e.target.value)}
                                    }></Form.Control>
                                </Form.Group>
                            </Row>
                            <Form.Group as={Row} className="mb-3">
                                <Form.Label column sm={2}>
                                    Sort By Name
                                </Form.Label>
                                <Col sm={2}>
                                    <Form.Control type="button" onClick={sortByName} value={nameSort}></Form.Control>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="mb-3">
                                <Form.Label column sm={2}>
                                    Sort By Album
                                </Form.Label>
                                <Col sm={2}>
                                    <Form.Control type="button" onClick={sortByAlbum} value={albumSort}></Form.Control>
                                </Col>
                            </Form.Group>
                        </Form>
                        <div>
                            <input type="button" disabled={prev} onClick={prevPage} value={"<"}/>
                            <label style={{marginTop: '10px', marginRight: '20px'
                            }}>   {page + 1}</label>
                            <input type="button" disabled={next} onClick={nextPage} value=">"/>
                        </div>
                        <div>
                    <AutoSizer>
                        {({width}) => (<List
                            width={width}
                            height={listHeight}
                            rowHeight={rowHeight}
                            rowCount={list.slice(pageRowCount * page, pageRowCount * page + pageRowCount).length}
                            rowRenderer={({key, index, style, parent}) => {
                                const temp = list.slice(pageRowCount * page, pageRowCount * page + pageRowCount);
                                const photo = temp[index];
                                console.log(photo)
                                return <div key={key} style={style}><PhotoItem height={style.height} photo={photo}/></div>
                            }}
                        />)}
                    </AutoSizer>
                        </div>
                    </div>
            }
        </Container>
    );
}

export default PhotoApp;